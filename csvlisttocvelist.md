# CSV List search for CVE using python with REST

* [https://python-poetry.org/docs/#installation](https://python-poetry.org/docs/#installation)

Install Python and Poetry Package manager

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```
Start project

```bash
poetry new csvlisttocvelist
```
Edit toml to add panda

```toml
[tool.poetry]
name = "csvlisttocvelist"
version = "0.1.0"
description = ""
authors = ["trentonknight <trentonknight@protonmail.com>"]

[tool.poetry.dependencies]
python = "*"
pandas = "*"
jupyterlab = "*"

[tool.poetry.dev-dependencies]
pytest = "*"

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
```
Initialize new project

```bash
poetry init
```
Fire up Jupyter Lab

```bash
jupyter lab
```
Install gruvbox theme

```bash
poetry add npm
poetry add nodejs
```
Or
```bash
pacman -Sy npm nodejs
```
Finally add theme

```bash
jupyter labextension install @rahlir/theme-gruvbox
```


